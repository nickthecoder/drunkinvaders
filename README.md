# Drunk Invaders

A new take, on an ancient classic.
Defend the Earth, Moon, and Mars from the incompetent invaders from space.
This time round, the invaders are tipsy, and rather than keep in a tight formation,
they bump into each other sending the spinning off in all directions.
This lack of discipline makes then a more dangerous foe, as they tend to dive-bomb
straight towards you.

## Screenshots

![classic](screenshots/classic.png)

It all looks fairly standard, but ...

![weird](screenshots/weird.png)

It gets weird!

![menu](screenshots/menu.png)

## Install

To play, you must first install [Tickle](http://nickthecoder/software/tickle).

## Status

The game is playable, but needs some more level before I'd call it finished.

## About

This was the first game written in Groovy using the Tickle Game Engine,
but has since been ported to [Feather](http://nickthecoder/software/feather),
which is a strongly typed scripting language.

Hopefully, it should be relatively easy to understand the code, and therefore
makes a good introduction to the Tickle game engine.

Powered by [Tickle](http://nickthecoder/software/tickle) and [LWJGL](https://www.lwjgl.org/).
